const person = {
    name: 'John',
    age: 30,
    gender: 'male',
};

const newPerson = Object.assign({}, person);
newPerson.age = 35;

console.log(person);
console.log(newPerson);


const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
}

function printCarInfo(car) {
    console.log(`Make:${car.make}, Model:${car.model}, Year:${car.year}`);
    if (car.year < 2001) {
        console.log("Машина занадто стара.");
    }
}

printCarInfo(car);


// const str = 'JavaScript is a high level programming language';
//
// function countWords(str) {
//     const words = str.split(/\s+/);
//     return words.length;
// }
//
// console.log(countWords(str));

//

// const str = 'I love life!';
//
// function reverseString(str) {
//     return str.split('').reverse().join('');
//
// }
// console.log(reverseString(str));


const str = 'JavaScript is awesome!';

function reverseWordsInString(str) {
    return str.split(' ').reverse().join(' ');
}

console.log(reverseWordsInString(str));
// 'tpircSavaJ si !emosewa' не выходит

